import gym, math, numpy
env = gym.make('CartPole-v0')

def scalingDownObservations(observation: list, environment, buckets : list = [(0.0,3.0), (0.0,6.0), (0.0,3.0), (0.0,6.0)]):
    # OBSERVATION layout
    # [0] --> cart position - from -4.8 to 4.8 (new sizing = [0,3])
    # [1] --> cart velocity - from -inf to inf (new sizing = [0,6])
    # [2] --> pole angle    - from -24 to 24 degrees (new sizing = [0,3])
    # [3] --> pole velocity - from -inf to inf ( new sizing = [0, 6])
    # general formula = ((max_new - min_new) / (max_old - min_old)) * ( v - max_old) + max_new
    upperBounds = env.observation_space.high
    lowerBounds = env.observation_space.low
    return [ int(round(
        (buckets[i][1] - buckets[i][0]) /
        (upperBounds[i] - lowerBounds[i]) *
        (observation[i] - upperBounds[i]) + buckets[i][1]))
        for i in range(len(observation))
        ]

def createQ(buckets: list = [(0.0,3.0), (0.0,6.0), (0.0,3.0), (0.0,6.0)]):
    Q = {}
    buckets = [(int(x), int(y)) for (x,y) in buckets]
    for i in range(max(buckets[0]) + 1):
        for z in range(max(buckets[1]) + 1):
            for d in range(max(buckets[2]) + 1):
                for h in range(max(buckets[3]) + 1):
                    Q[str([i,z,d,h])] = {0 : 0.0, 1 : 0.0}
    return Q

def chooseLeftRight(Q: dict, currentObservation: list):
    maxItem = max(Q[str(currentObservation)], key = Q[str(currentObservation)].get)
    return maxItem

def adjAlpha(alpha: float, episode: int, reg: float):
    return max(alpha, min(1.0, 1.0 - math.log10((episode + 1) / reg)))

def adjEpsilon(epsilon: float, episode: int, reg: float):
    #can be removed
    return max(epsilon, min(1.0, 1.0 - math.log10((episode + 1) / reg)))

def updateQ(Q: dict, reward: float, alpha: float, gamma: float, currentObservation: list, newObservation: list, action: int):
    Q[str(currentObservation)][action]  += alpha * (reward + gamma * max(Q[str(newObservation)].values()) - Q[str(currentObservation)][action])
    print(str(currentObservation))
    #print(Q[str(currentObservation)][action])

def run(episodes: int = 10):
    Q = createQ()
    alpha = 0.1
    epsilon = 0.1
    gamma = 1.0
    reg = 25.0
    overallScore = []

    for e in range(episodes):
        currentObservation = scalingDownObservations(env.reset(), env)
        done = False
        alpha = adjAlpha(alpha, e, reg)
        epsilon = adjEpsilon(epsilon, e, reg)
        score = 0

        while not done:
            #env.render()
            action = chooseLeftRight(Q, currentObservation)
            observation, reward, done, info = env.step(action)
            newObservation = scalingDownObservations(observation, env)
            updateQ(Q, reward, alpha, gamma, currentObservation, newObservation, action)
            currentObservation = newObservation
            score += 1

        overallScore.append(score)
        #if e % 100 == 0:
            #print('MEDIA DE SCORE ' + str(numpy.mean(score)))
if __name__ == "__main__":
    run(10000)
